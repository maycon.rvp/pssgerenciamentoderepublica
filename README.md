# Trabalho Final
- Equipe: Igor, Maycon, Pedro Henrique, Lucas Marques e Wérikis


Convenções dos prefixos das variáveis de componentes Swing:

| Componentes     | Prefixo |
| --------------- | ------- |
| Label           | lbl     |
| Button          | btn     |
| Check Box       | chb     |
| Radio Button    | rbt     |
| Button Group    | btg     |
| Combo Box       | cbb     |
| List            | lst     |
| Text Field      | txf     |
| Text Area       | txa     |
| Formatted Field | ffd     |
| Password Field  | pwd     |
| Spinner         | spr     |
| Table           | tbl     |
| Menu Item       | mit     |

