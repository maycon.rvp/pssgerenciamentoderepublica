package com.pss.pssgerenciamentoderepublica.model;

import com.pss.pssgerenciamentoderepublica.model.state.morador.MoradorState;

public class Morador {

    private Integer id;
    private String nome;
    private String apelido;
    private String telefone;
    private String telefoneResponsavel1;
    private String telefoneResponsavel2;
    private String cpf;
    private String linkRedeSocial;

    private Republica republicaAtual;
    private MoradorState state;

    public Morador() {
    }

    public Morador(String nome, String apelido, String telefone, String telefoneResponsavel1, String telefoneResponsavel2, String cpf, String linkRedeSocial, Republica republicaAtual, MoradorState state) {
        this.nome = nome;
        this.apelido = apelido;
        this.telefone = telefone;
        this.telefoneResponsavel1 = telefoneResponsavel1;
        this.telefoneResponsavel2 = telefoneResponsavel2;
        this.cpf = cpf;
        this.linkRedeSocial = linkRedeSocial;
        this.republicaAtual = republicaAtual;
        this.state = state;
    }
    
    public Morador(Integer id, String nome, String apelido, String telefone, String telefoneResponsavel1, String telefoneResponsavel2, String cpf, Republica republica, MoradorState state) {
        this.id = id;
        this.nome = nome;
        this.apelido = apelido;
        this.telefone = telefone;
        this.telefoneResponsavel1 = telefoneResponsavel1;
        this.telefoneResponsavel2 = telefoneResponsavel2;
        this.cpf = cpf;
        this.republicaAtual = republica;
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefoneResponsavel1() {
        return telefoneResponsavel1;
    }

    public void setTelefoneResponsavel1(String telefoneResponsavel1) {
        this.telefoneResponsavel1 = telefoneResponsavel1;
    }

    public String getTelefoneResponsavel2() {
        return telefoneResponsavel2;
    }

    public void setTelefoneResponsavel2(String telefoneResponsavel2) {
        this.telefoneResponsavel2 = telefoneResponsavel2;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getLinkRedeSocial() {
        return linkRedeSocial;
    }

    public void setLinkRedeSocial(String linkRedeSocial) {
        this.linkRedeSocial = linkRedeSocial;
    }

    public Republica getRepublicaAtual() {
        return republicaAtual;
    }

    public void setRepublicaAtual(Republica republicaAtual) {
        this.republicaAtual = republicaAtual;
    }

    public MoradorState getState() {
        return state;
    }

    public void setState(MoradorState state) {
        this.state = state;
    }

   

}
