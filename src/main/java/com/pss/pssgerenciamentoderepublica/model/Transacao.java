package com.pss.pssgerenciamentoderepublica.model;

import com.pss.pssgerenciamentoderepublica.model.enumerate.LancamentoEnum;
import com.pss.pssgerenciamentoderepublica.model.enumerate.PeriodicidadeEnum;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class Transacao {
    
    private Integer id;
    private LancamentoEnum lancamento;
    private String descricao;
    private LocalDate dataVencimento;
    private BigDecimal valor;
    private BigDecimal valorParcela;
    private Double roteio;
    private PeriodicidadeEnum periodicidade;
    private List<Morador> moradoresParticipantes;

    public Transacao() {
    }

    public Transacao(LancamentoEnum lancamento, String descricao, LocalDate dataVencimento, BigDecimal valor, BigDecimal valorParcela, Double roteio, PeriodicidadeEnum periodicidade, List<Morador> moradoresParticipantes) {
        this.lancamento = lancamento;
        this.descricao = descricao;
        this.dataVencimento = dataVencimento;
        this.valor = valor;
        this.valorParcela = valorParcela;
        this.roteio = roteio;
        this.periodicidade = periodicidade;
        this.moradoresParticipantes = moradoresParticipantes;
    }

    public Transacao(Integer id, LancamentoEnum lancamento, String descricao, LocalDate dataVencimento, BigDecimal valor, BigDecimal valorParcela, Double roteio, PeriodicidadeEnum periodicidade, List<Morador> moradoresParticipantes) {
        this.id = id;
        this.lancamento = lancamento;
        this.descricao = descricao;
        this.dataVencimento = dataVencimento;
        this.valor = valor;
        this.valorParcela = valorParcela;
        this.roteio = roteio;
        this.periodicidade = periodicidade;
        this.moradoresParticipantes = moradoresParticipantes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public LancamentoEnum getLancamento() {
        return lancamento;
    }

    public void setLancamento(LancamentoEnum lancamento) {
        this.lancamento = lancamento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(LocalDate dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(BigDecimal valorParcela) {
        this.valorParcela = valorParcela;
    }

    public Double getRoteio() {
        return roteio;
    }

    public void setRoteio(Double roteio) {
        this.roteio = roteio;
    }

    public PeriodicidadeEnum getPeriodicidade() {
        return periodicidade;
    }

    public void setPeriodicidade(PeriodicidadeEnum periodicidade) {
        this.periodicidade = periodicidade;
    }

    public List<Morador> getMoradoresParticipantes() {
        return moradoresParticipantes;
    }

    public void setMoradoresParticipantes(List<Morador> moradoresParticipantes) {
        this.moradoresParticipantes = moradoresParticipantes;
    }

    
    
    
    
}
