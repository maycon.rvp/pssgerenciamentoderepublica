package com.pss.pssgerenciamentoderepublica.model;

import com.pss.pssgerenciamentoderepublica.model.enumerate.FeedbackEnum;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Feedback {

    private Integer id;
    private FeedbackEnum tipo;
    private LocalDate dataRealizada;
    private LocalDate dataFinalizada;
    private String descricao;

    private Morador autor;
    private Republica republica;
    private List<Morador> moradoresAtuais;
    private List<Morador> moradoresEnvolvidos;

    public Feedback() {
    }

    public Feedback(Integer id, FeedbackEnum tipo, String descricao, Morador autor, Republica republica) {
        this.id = id;
        this.tipo = tipo;
        this.descricao = descricao;
        this.autor = autor;
        this.republica = republica;
        this.moradoresAtuais = republica.getMoradores();
        this.dataRealizada = LocalDate.now();
        this.dataFinalizada= null;
        this.moradoresEnvolvidos= new ArrayList<>(); 
    }
     
    public Feedback(FeedbackEnum tipo, String descricao, Morador autor, Republica republica) {
        this.tipo = tipo;
        this.descricao = descricao;
        this.autor = autor;
        this.republica=republica;
        this.moradoresAtuais = republica.getMoradores();
        this.dataRealizada = LocalDate.now();
        this.dataFinalizada= null;
        this.moradoresEnvolvidos= new ArrayList<>();     
    }
    
    public Integer getId() {
        return id;
    }
    
    public FeedbackEnum getTipo() {
        return tipo;
    }

    public void setTipo(FeedbackEnum tipo) {
        this.tipo = tipo;
    }

    public LocalDate getDataRealizada() {
        return dataRealizada;
    }

    public void setDataRealizada(LocalDate dataRealizada) {
        this.dataRealizada = dataRealizada;
    }

    public LocalDate getDataFinalizada() {
        return dataFinalizada;
    }

    public void setDataFinalizada(LocalDate dataFinalizada) {
        this.dataFinalizada = dataFinalizada;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Morador getAutor() {
        return autor;
    }

    public void setAutor(Morador autor) {
        this.autor = autor;
    }

    public Republica getRepublica() {
        return republica;
    }

    public void setRepublica(Republica republica) {
        this.republica = republica;
    }

    public List<Morador> getMoradoresAtuais() {
        return moradoresAtuais;
    }

    public void setMoradoresAtuais(List<Morador> moradoresAtuais) {
        this.moradoresAtuais = moradoresAtuais;
    }

    public List<Morador> getMoradoresEnvolvidos() {
        return moradoresEnvolvidos;
    }

    public void setMoradoresEnvolvidos(List<Morador> moradoresEnvolvidos) {
        this.moradoresEnvolvidos = moradoresEnvolvidos;
    }
    
    

}
