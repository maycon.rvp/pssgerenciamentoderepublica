package com.pss.pssgerenciamentoderepublica.model.state.republica;

import com.pss.pssgerenciamentoderepublica.model.Republica;

public class RepublicaDisponivelState extends RepublicaState {

    public RepublicaDisponivelState(Republica republica) {
        super(republica);
    }
    
    @Override
    public void republicaCheia() {
        this.republica.setState(new RepublicaCheiaState(republica));
    }
}
