package com.pss.pssgerenciamentoderepublica.model.state.morador;

import com.pss.pssgerenciamentoderepublica.model.Morador;

public abstract class MoradorState {
    
    protected Morador morador;

    public MoradorState(Morador morador) {
        this.morador = morador;
    }
    
    public void incluidoNaRepublica() {
        throw new RuntimeException("Você não pode passar para o estado incluído no estado atual!");
    }
    
    public void promovidoNaRepublica() {
        throw new RuntimeException("Você não pode passar para o estado promovido no estado atual!");
    }
    
    public void rebaixadoNaRepublica() {
        throw new RuntimeException("Você não pode passar para o estado rebaixado no estado atual!");
    }
    
    public void removidoDaRepublica() {
        throw new RuntimeException("Você não pode passar para o estado removido no estado atual!");
    }
    
}
