package com.pss.pssgerenciamentoderepublica.model.state.morador;

import com.pss.pssgerenciamentoderepublica.model.Morador;

public class MoradorComumState extends MoradorState {

    public MoradorComumState(Morador morador) {
        super(morador);
    }
    
    @Override
    public void promovidoNaRepublica() {
        this.morador.setState(new RepresentanteState(morador));
    }
    
    @Override
    public void removidoDaRepublica() {
        this.morador.setState(new SemTetoState(morador));
    }
    
}
