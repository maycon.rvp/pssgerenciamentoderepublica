package com.pss.pssgerenciamentoderepublica.model.state.republica;

import com.pss.pssgerenciamentoderepublica.model.Republica;

public class RepublicaCheiaState extends RepublicaState {

    public RepublicaCheiaState(Republica republica) {
        super(republica);
    }
    
    @Override
    public void republicaDisponivel() {
        this.republica.setState(new RepublicaDisponivelState(republica));
    }
}
