package com.pss.pssgerenciamentoderepublica.model.state.morador;

import com.pss.pssgerenciamentoderepublica.model.Morador;

public class SemTetoState extends MoradorState {

    public SemTetoState(Morador morador) {
        super(morador);
    }
    
    @Override
    public void incluidoNaRepublica() {
        this.morador.setState(new MoradorComumState(morador));
    }
    
}
