package com.pss.pssgerenciamentoderepublica.model.state.morador;

import com.pss.pssgerenciamentoderepublica.model.Morador;

public class RepresentanteState extends MoradorState {

    public RepresentanteState(Morador morador) {
        super(morador);
    }
    
    @Override
    public void rebaixadoNaRepublica() {
        this.morador.setState(new MoradorComumState(morador));
    }
    
}
