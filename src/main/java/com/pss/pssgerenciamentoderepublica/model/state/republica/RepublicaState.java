package com.pss.pssgerenciamentoderepublica.model.state.republica;

import com.pss.pssgerenciamentoderepublica.model.Republica;

public abstract class RepublicaState {
    
    protected Republica republica;

    public RepublicaState(Republica republica) {
        this.republica = republica;
    }
    
    public void republicaDisponivel() {
        throw new RuntimeException("Você não pode passar para o estado disponível no estado atual!");
    }
    
    public void republicaCheia() {
        throw new RuntimeException("Você não pode passar para o estado cheia no estado atual!");
    }
}
