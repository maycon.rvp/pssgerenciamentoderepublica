package com.pss.pssgerenciamentoderepublica.model;

import java.time.LocalDate;
import java.util.List;

public class Tarefa {
    
    private Integer id;
    private LocalDate dataAgendamento;
    private List<Morador> moradoresResponsaveis;
    private String descricao;
    private LocalDate dataTermino;
    private Boolean finalizada;

    public Tarefa() {
    }

    public Tarefa(LocalDate dataAgendamento, List<Morador> moradoresResponsaveis, String descricao, LocalDate dataTermino, Boolean finalizada) {
        this.dataAgendamento = dataAgendamento;
        this.moradoresResponsaveis = moradoresResponsaveis;
        this.descricao = descricao;
        this.dataTermino = dataTermino;
        this.finalizada = finalizada;
    }

    public Tarefa(Integer id, LocalDate dataAgendamento, List<Morador> moradoresResponsaveis, String descricao, LocalDate dataTermino, Boolean finalizada) {
        this.id = id;
        this.dataAgendamento = dataAgendamento;
        this.moradoresResponsaveis = moradoresResponsaveis;
        this.descricao = descricao;
        this.dataTermino = dataTermino;
        this.finalizada = finalizada;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public LocalDate getDataAgendamento() {
        return dataAgendamento;
    }

    public void setDataAgendamento(LocalDate dataAgendamento) {
        this.dataAgendamento = dataAgendamento;
    }

    public List<Morador> getMoradoresResponsaveis() {
        return moradoresResponsaveis;
    }

    public void setMoradoresResponsaveis(List<Morador> moradoresResponsaveis) {
        this.moradoresResponsaveis = moradoresResponsaveis;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public LocalDate getDataTermino() {
        return dataTermino;
    }

    public void setDataTermino(LocalDate dataTermino) {
        this.dataTermino = dataTermino;
    }

    public Boolean getFinalizada() {
        return finalizada;
    }

    public void setFinalizada(Boolean finalizada) {
        this.finalizada = finalizada;
    }
    
    
}
