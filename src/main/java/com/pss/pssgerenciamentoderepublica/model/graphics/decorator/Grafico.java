package com.pss.pssgerenciamentoderepublica.model.graphics.decorator;

import com.pss.pssgerenciamentoderepublica.model.graphics.Componente;
import org.jfree.data.category.DefaultCategoryDataset;

public abstract class Grafico extends Componente {
    
    protected Componente componente;
    protected DefaultCategoryDataset defaultDataset;

    public Grafico(DefaultCategoryDataset defaultDataset) {
        this.defaultDataset = defaultDataset;
    }
    
}