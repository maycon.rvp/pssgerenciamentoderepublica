package com.pss.pssgerenciamentoderepublica.model.graphics.decorator;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class GraficoColuna extends Grafico {

    public GraficoColuna(DefaultCategoryDataset defaultDataset) {
        super(defaultDataset);
    }

    @Override
    public JFreeChart construir() throws Exception {
        JFreeChart grafico = criarGraficoColuna(defaultDataset);
        return grafico;
    }
    
    private JFreeChart criarGraficoColuna(CategoryDataset defaultDataset) {
        JFreeChart grafico = ChartFactory.createBarChart("", "", "", defaultDataset, PlotOrientation.VERTICAL, true, false, false);
        return grafico;
    }
    
}
