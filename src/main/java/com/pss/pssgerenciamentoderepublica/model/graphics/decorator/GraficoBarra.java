package com.pss.pssgerenciamentoderepublica.model.graphics.decorator;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class GraficoBarra extends Grafico {

    public GraficoBarra(DefaultCategoryDataset defaultDataset) {
        super(defaultDataset);
    }

    @Override
    public JFreeChart construir() throws Exception {
        JFreeChart grafico = criarGraficoBarra(defaultDataset);
        return grafico;
    }
    
    private JFreeChart criarGraficoBarra(CategoryDataset defaultDataset) {
        JFreeChart grafico = ChartFactory.createBarChart("", "", "", defaultDataset, PlotOrientation.HORIZONTAL, true, false, false);
        return grafico;
    }
 
}
