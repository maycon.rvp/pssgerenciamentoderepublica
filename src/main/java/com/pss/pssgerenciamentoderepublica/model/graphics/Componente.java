package com.pss.pssgerenciamentoderepublica.model.graphics;

import org.jfree.chart.JFreeChart;

public abstract class Componente {
    
    public abstract JFreeChart construir() throws Exception;
    
}
