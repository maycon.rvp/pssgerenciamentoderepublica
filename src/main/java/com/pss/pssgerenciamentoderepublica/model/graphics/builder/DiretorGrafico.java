package com.pss.pssgerenciamentoderepublica.model.graphics.builder;

import com.pss.pssgerenciamentoderepublica.model.graphics.Componente;

public class DiretorGrafico {
    
    private final ConstrutorGrafico construtor;

    public DiretorGrafico(ConstrutorGrafico construtor) {
        this.construtor = construtor;
    }
    
    public Componente construirGrafico() {
        construtor.criarGrafico();
        return construtor.getComponente();
    }
}
