package com.pss.pssgerenciamentoderepublica.model.graphics.decorator;

import com.pss.pssgerenciamentoderepublica.model.graphics.Componente;
import org.jfree.chart.JFreeChart;

public class Titulo extends ComponenteGrafico {
    
    private final String titulo;

    public Titulo(String titulo, Componente componenteDecorado) {
        super(componenteDecorado);
        this.titulo = titulo;
    }

    @Override
    public JFreeChart construir() throws Exception {
        JFreeChart grafico = componenteDecorado.construir();
        grafico.setTitle(titulo);
        
        return grafico;
    }
    
}
