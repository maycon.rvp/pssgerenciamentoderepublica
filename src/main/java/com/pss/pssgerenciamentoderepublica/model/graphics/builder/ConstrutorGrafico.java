package com.pss.pssgerenciamentoderepublica.model.graphics.builder;

import com.pss.pssgerenciamentoderepublica.model.graphics.Componente;

public abstract class ConstrutorGrafico {
    
    protected Componente componente;

    public ConstrutorGrafico() {
    }
    
    public abstract void criarGrafico();
    
    public abstract void inserirLegenda(boolean visibilidade);
    
    public abstract void inserirRotuloDados(boolean mostrar);
    
    public abstract void inserirTituloEixo(String descricaoX, String descricaoY);
    
    public abstract void inserirTitulo(String titulo);
    
    public Componente getComponente() {
        return componente;
    }
}
