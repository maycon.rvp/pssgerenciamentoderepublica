package com.pss.pssgerenciamentoderepublica.model.graphics.decorator;

import com.pss.pssgerenciamentoderepublica.model.graphics.Componente;
import org.jfree.chart.JFreeChart;

public class Legenda extends ComponenteGrafico {
    
    private final boolean visibilidade;

    public Legenda(boolean visibilidade, Componente componenteDecorado) {
        super(componenteDecorado);
        this.visibilidade = visibilidade;
    }

    @Override
    public JFreeChart construir() throws Exception {
        JFreeChart chart = componenteDecorado.construir();
        chart.getLegend().setVisible(visibilidade);
        
        return chart;
    }
    
}
