package com.pss.pssgerenciamentoderepublica.model.graphics.decorator;

import com.pss.pssgerenciamentoderepublica.model.graphics.Componente;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.ui.TextAnchor;

public class RotuloDados extends ComponenteGrafico {
    
    private final boolean mostrar;

    public RotuloDados(boolean mostrar, Componente componenteDecorado) {
        super(componenteDecorado);
        this.mostrar = mostrar;
    }

    @Override
    public JFreeChart construir() throws Exception {
        JFreeChart grafico = componenteDecorado.construir();
        CategoryPlot plot = grafico.getCategoryPlot();
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.CENTER));
        renderer.setBaseItemLabelsVisible(mostrar);
        
        return grafico;
    }
    
}
