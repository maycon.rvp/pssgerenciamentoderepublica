package com.pss.pssgerenciamentoderepublica.model.graphics.decorator;

import com.pss.pssgerenciamentoderepublica.model.graphics.Componente;
import org.jfree.chart.JFreeChart;

public class DescricaoY extends ComponenteGrafico {
    
    private final String descricao;

    public DescricaoY(String descricao, Componente componenteDecorado) {
        super(componenteDecorado);
        this.descricao = descricao;
    }

    @Override
    public JFreeChart construir() throws Exception {
        JFreeChart grafico = componenteDecorado.construir();
        grafico.getCategoryPlot().getRangeAxis().setLabel(descricao);
        
        return grafico;
    }
    
}
