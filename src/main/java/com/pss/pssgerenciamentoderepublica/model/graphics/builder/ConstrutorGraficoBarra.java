package com.pss.pssgerenciamentoderepublica.model.graphics.builder;

import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.GraficoBarra;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.Legenda;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.RotuloDados;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.Titulo;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.DescricaoX;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.DescricaoY;
import org.jfree.data.category.DefaultCategoryDataset;

public class ConstrutorGraficoBarra extends ConstrutorGrafico {
    
    private final DefaultCategoryDataset defaultDataset;

    public ConstrutorGraficoBarra(DefaultCategoryDataset defaultDataset) {
        this.defaultDataset = defaultDataset;
    }

    @Override
    public void criarGrafico() {
        componente = new GraficoBarra(defaultDataset);
    }

    @Override
    public void inserirLegenda(boolean visibility) {
        componente = new Legenda(visibility, componente);
    }

    @Override
    public void inserirRotuloDados(boolean show) {
        componente = new RotuloDados(show, componente);
    }

    @Override
    public void inserirTituloEixo(String xDescription, String yDescription) {
        componente = new DescricaoX(xDescription, componente);
        componente = new DescricaoY(yDescription, componente);
    }

    @Override
    public void inserirTitulo(String title) {
        componente = new Titulo(title, componente);
    }
    
}
