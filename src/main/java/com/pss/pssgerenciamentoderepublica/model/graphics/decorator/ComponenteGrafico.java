package com.pss.pssgerenciamentoderepublica.model.graphics.decorator;

import com.pss.pssgerenciamentoderepublica.model.graphics.Componente;

public abstract class ComponenteGrafico extends Componente {
    
    protected Componente componenteDecorado;

    public ComponenteGrafico(Componente componenteDecorado) {
        this.componenteDecorado = componenteDecorado;
    }
    
}
