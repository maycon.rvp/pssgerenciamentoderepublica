package com.pss.pssgerenciamentoderepublica.model.graphics.builder;

import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.Legenda;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.GraficoColuna;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.RotuloDados;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.Titulo;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.DescricaoX;
import com.pss.pssgerenciamentoderepublica.model.graphics.decorator.DescricaoY;
import org.jfree.data.category.DefaultCategoryDataset;

public class ConstrutorGraficoColuna extends ConstrutorGrafico {
    
    private final DefaultCategoryDataset defaultDataset;

    public ConstrutorGraficoColuna(DefaultCategoryDataset defaultDataset) {
        this.defaultDataset = defaultDataset;
    }

    @Override
    public void criarGrafico() {
        componente = new GraficoColuna(defaultDataset);
    }

    @Override
    public void inserirLegenda(boolean visibilidade) {
        componente = new Legenda(visibilidade, componente);
    }

    @Override
    public void inserirRotuloDados(boolean mostrar) {
        componente = new RotuloDados(mostrar, componente);
    }

    @Override
    public void inserirTituloEixo(String descricaoX, String descricaoY) {
        componente = new DescricaoX(descricaoX, componente);
        componente = new DescricaoY(descricaoY, componente);
    }

    @Override
    public void inserirTitulo(String titulo) {
        componente = new Titulo(titulo, componente);
    }
    
}
