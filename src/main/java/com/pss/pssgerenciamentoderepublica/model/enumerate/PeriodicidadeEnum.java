package com.pss.pssgerenciamentoderepublica.model.enumerate;

public enum PeriodicidadeEnum {
    MENSAL("Mensal"),
    SEMANAL("Semana"),
    DESPESA_UNICA("Semana única");
    
    private String descricao;

    private PeriodicidadeEnum(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
    
}
