package com.pss.pssgerenciamentoderepublica.model.enumerate;

public enum LancamentoEnum {
    RECEITA("Receita"),
    DESPESA("Despesa");
    
    private String descricao;
    
    LancamentoEnum(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
    
}
