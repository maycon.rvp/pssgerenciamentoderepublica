
package com.pss.pssgerenciamentoderepublica.model.enumerate;


public enum FeedbackEnum {
   
    RECLAMACAO("Reclamação"),
    SUGESTAO("Sugestão");
    
    private String descricao;
    
    FeedbackEnum(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
    
}
