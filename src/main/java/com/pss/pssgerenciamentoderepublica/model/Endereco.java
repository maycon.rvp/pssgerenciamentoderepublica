package com.pss.pssgerenciamentoderepublica.model;

public class Endereco {
    private Integer id;
    private String logradouro;
    private String cep;
    private String bairro;
    private String pontoReferencia;
    private String localizacaoGeografica;

    public Endereco() {
    }

    public Endereco(String logradouro, String cep, String bairro, String pontoReferencia) {
        this.logradouro = logradouro;
        this.cep = cep;
        this.bairro = bairro;
        this.pontoReferencia = pontoReferencia;
    }

    public Endereco(String logradouro, String cep, String bairro, String pontoReferencia, String localizacaoGeografica) {
        this.logradouro = logradouro;
        this.cep = cep;
        this.bairro = bairro;
        this.pontoReferencia = pontoReferencia;
        this.localizacaoGeografica = localizacaoGeografica;
    }

    public Endereco(Integer id, String logradouro, String cep, String bairro, String pontoReferencia) {
        this.id = id;
        this.logradouro = logradouro;
        this.cep = cep;
        this.bairro = bairro;
        this.pontoReferencia = pontoReferencia;
    }
    
    public Endereco(Integer id, String logradouro, String cep, String bairro, String pontoReferencia, String localizacaoGeografica) {
        this.id = id;
        this.logradouro = logradouro;
        this.cep = cep;
        this.bairro = bairro;
        this.pontoReferencia = pontoReferencia;
        this.localizacaoGeografica = localizacaoGeografica;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getPontoReferencia() {
        return pontoReferencia;
    }

    public void setPontoReferencia(String pontoReferencia) {
        this.pontoReferencia = pontoReferencia;
    }

    public String getLocalizacaoGeografica() {
        return localizacaoGeografica;
    }

    public void setLocalizacaoGeografica(String localizacaoGeografica) {
        this.localizacaoGeografica = localizacaoGeografica;
    }
    
    
    
}
