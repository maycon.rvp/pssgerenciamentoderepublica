package com.pss.pssgerenciamentoderepublica.model;

import com.pss.pssgerenciamentoderepublica.model.state.republica.RepublicaState;
import java.time.LocalDate;
import java.util.List;

public class Republica {
    
    private Integer id;
    private String nome;
    private String vantagens;
    private String codigoDeEtica;
    private LocalDate dataFundacao;
    private LocalDate dataExtincao;
    private int totalVagas;
    private int vagasOcupadas;
    private int vagasDisponiveis;
    
    private Endereco endereco;
    private List<Morador> moradores;
    private RepublicaState state;

    public Republica() {
    }

    public Republica(String nome, String vantagens, LocalDate dataFundacao, LocalDate dataExtincao, int totalVagas, int vagasOcupadas, int vagasDisponiveis, Endereco endereco, List<Morador> moradores, RepublicaState state) {
        this.nome = nome;
        this.vantagens = vantagens;
        this.dataFundacao = dataFundacao;
        this.dataExtincao = dataExtincao;
        this.totalVagas = totalVagas;
        this.vagasOcupadas = vagasOcupadas;
        this.vagasDisponiveis = vagasDisponiveis;
        this.endereco = endereco;
        this.moradores = moradores;
        this.state = state;
    }
    
    public Republica(String nome, String vantagens, String codigoDeEtica, LocalDate dataFundacao, LocalDate dataExtincao, int totalVagas, int vagasOcupadas, int vagasDisponiveis, Endereco endereco, List<Morador> moradores, RepublicaState state) {
        this.nome = nome;
        this.vantagens = vantagens;
        this.codigoDeEtica = codigoDeEtica;
        this.dataFundacao = dataFundacao;
        this.dataExtincao = dataExtincao;
        this.totalVagas = totalVagas;
        this.vagasOcupadas = vagasOcupadas;
        this.vagasDisponiveis = vagasDisponiveis;
        this.endereco = endereco;
        this.moradores = moradores;
        this.state = state;
    }

    public Republica(Integer id, String nome, String vantagens, LocalDate dataFundacao, LocalDate dataExtincao, int totalVagas, int vagasOcupadas, int vagasDisponiveis, Endereco endereco, List<Morador> moradores, RepublicaState state) {
        this.id = id;
        this.nome = nome;
        this.vantagens = vantagens;
        this.dataFundacao = dataFundacao;
        this.dataExtincao = dataExtincao;
        this.totalVagas = totalVagas;
        this.vagasOcupadas = vagasOcupadas;
        this.vagasDisponiveis = vagasDisponiveis;
        this.endereco = endereco;
        this.moradores = moradores;
        this.state = state;
    }

    public Republica(Integer id, String nome, String vantagens, String codigoDeEtica, LocalDate dataFundacao, LocalDate dataExtincao, int totalVagas, int vagasOcupadas, int vagasDisponiveis, Endereco endereco, List<Morador> moradores, RepublicaState state) {
        this.id = id;
        this.nome = nome;
        this.vantagens = vantagens;
        this.codigoDeEtica = codigoDeEtica;
        this.dataFundacao = dataFundacao;
        this.dataExtincao = dataExtincao;
        this.totalVagas = totalVagas;
        this.vagasOcupadas = vagasOcupadas;
        this.vagasDisponiveis = vagasDisponiveis;
        this.endereco = endereco;
        this.moradores = moradores;
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getVantagens() {
        return vantagens;
    }

    public void setVantagens(String vantagens) {
        this.vantagens = vantagens;
    }

    public String getCodigoDeEtica() {
        return codigoDeEtica;
    }

    public void setCodigoDeEtica(String codigoDeEtica) {
        this.codigoDeEtica = codigoDeEtica;
    }

    public LocalDate getDataFundacao() {
        return dataFundacao;
    }

    public void setDataFundacao(LocalDate dataFundacao) {
        this.dataFundacao = dataFundacao;
    }

    public LocalDate getDataExtincao() {
        return dataExtincao;
    }

    public void setDataExtincao(LocalDate dataExtincao) {
        this.dataExtincao = dataExtincao;
    }

    public int getTotalVagas() {
        return totalVagas;
    }

    public void setTotalVagas(int totalVagas) {
        this.totalVagas = totalVagas;
    }

    public int getVagasOcupadas() {
        return vagasOcupadas;
    }

    public void setVagasOcupadas(int vagasOcupadas) {
        this.vagasOcupadas = vagasOcupadas;
    }

    public int getVagasDisponiveis() {
        return vagasDisponiveis;
    }

    public void setVagasDisponiveis(int vagasDisponiveis) {
        this.vagasDisponiveis = vagasDisponiveis;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public List<Morador> getMoradores() {
        return moradores;
    }

    public void setMoradores(List<Morador> moradores) {
        this.moradores = moradores;
    }

    public RepublicaState getState() {
        return state;
    }

    public void setState(RepublicaState state) {
        this.state = state;
    }
    
}
