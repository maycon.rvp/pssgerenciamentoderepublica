package com.pss.pssgerenciamentoderepublica.model;

public class RedeSocial {
    
    private Integer id;
    private String nome;
    private String link;

    public RedeSocial() {
    }

    public RedeSocial(String nome, String link) {
        this.nome = nome;
        this.link = link;
    }

    public RedeSocial(Integer id, String nome, String link) {
        this.id = id;
        this.nome = nome;
        this.link = link;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
}
