package com.pss.pssgerenciamentoderepublica.presenter;

import com.pss.pssgerenciamentoderepublica.model.Usuario;
import com.pss.pssgerenciamentoderepublica.view.LoginView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginPresenter {
    
    private final LoginView view;
    private Usuario usuario;

    public LoginPresenter() {
        view = new LoginView();
        
        view.setLocationRelativeTo(null);
        view.setVisible(true);
        
        initListeners();
    }
    
    private void initListeners() {
        view.getBtnAcessar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new PrincipalPresenter(usuario);
                view.dispose();
            }
        });
        
        view.getBtnCancelar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limparCampos();
            }
        });
        
        view.getBtnNovoUsuario().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new NovoUsuarioPresenter();
                view.dispose();
            }
        });
    }
    
    private void limparCampos() {
        view.getTxfUsuario().setText("");
        view.getPwdSenha().setText("");
    }
    
    public LoginView getView() {
        return view;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
}
