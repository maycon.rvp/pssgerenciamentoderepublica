
package com.pss.pssgerenciamentoderepublica.presenter.state.principal;

import com.pss.pssgerenciamentoderepublica.presenter.PrincipalPresenter;


public class PrincipalDefaultState extends PrincipalPresenterState{
    
    public PrincipalDefaultState(PrincipalPresenter presenter) {
        super(presenter);
        
        configurarView();
    }
    
    private void configurarView(){

        presenter.getView().getMitManterRepublica().setEnabled(false);
        presenter.getView().getMitManterMoradores().setEnabled(false);
        presenter.getView().getMitConfirmarFeedBack().setEnabled(false);
        presenter.getView().getMitAceitarConvite().setEnabled(false);
        presenter.getView().getMitCriarRepublica().setEnabled(true);
        presenter.getView().getMitBuscarVagas().setEnabled(true);
        presenter.getView().getMitConsultaResultadoMensal().setEnabled(false);
        presenter.getView().getMitManterFeedBack().setEnabled(false);
        presenter.getView().getMitManterConvites().setEnabled(false);

        presenter.getView().getMitManterPerfil().setEnabled(false);

        presenter.getView().getMitManterTarefa().setEnabled(false);
        presenter.getView().getMitRegistrarConclusãoTarefa().setEnabled(false);

        presenter.getView().getMitManterContas().setEnabled(false);
        presenter.getView().getMitRealizarEstorno().setEnabled(false);
        presenter.getView().getMitConsultarContas().setEnabled(false);
        presenter.getView().getMitRegistrarLancamento().setEnabled(false);

        presenter.getView().getMitFechar().setEnabled(true);
    }
    
}
