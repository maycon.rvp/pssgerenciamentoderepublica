
package com.pss.pssgerenciamentoderepublica.presenter.state.principal;

import com.pss.pssgerenciamentoderepublica.presenter.PrincipalPresenter;


public class PrincipalMoradorState extends PrincipalPresenterState{
    
    public PrincipalMoradorState(PrincipalPresenter presenter) {
        super(presenter);
        
        configurarView();
    }

    private void configurarView() {
        presenter.getView().getMitManterRepublica().setEnabled(false);
        presenter.getView().getMitManterMoradores().setEnabled(false);
        presenter.getView().getMitConfirmarFeedBack().setEnabled(false);
        presenter.getView().getMitCriarRepublica().setEnabled(false);
        presenter.getView().getMitBuscarVagas().setEnabled(false);
        presenter.getView().getMitConsultaResultadoMensal().setEnabled(true);
        presenter.getView().getMitManterFeedBack().setEnabled(true);
        presenter.getView().getMitManterConvites().setEnabled(false);

        presenter.getView().getMitManterPerfil().setEnabled(true);
        presenter.getView().getMitAceitarConvite().setEnabled(true);

        presenter.getView().getMitManterTarefa().setEnabled(false);
        presenter.getView().getMitRegistrarConclusãoTarefa().setEnabled(true);

        presenter.getView().getMitManterContas().setEnabled(false);
        presenter.getView().getMitRealizarEstorno().setEnabled(false);
        presenter.getView().getMitConsultarContas().setEnabled(true);
        presenter.getView().getMitRegistrarLancamento().setEnabled(true);

        presenter.getView().getMitFechar().setEnabled(true);
    }
    

}
