
package com.pss.pssgerenciamentoderepublica.presenter.state.principal;

import com.pss.pssgerenciamentoderepublica.presenter.PrincipalPresenter;

public class PrincipalRepresentanteState extends PrincipalPresenterState{
    
    public PrincipalRepresentanteState(PrincipalPresenter presenter) {
        super(presenter);
        
        configurarView();
    }

    private void configurarView() {
        presenter.getView().getMitManterRepublica().setEnabled(true);
        presenter.getView().getMitManterMoradores().setEnabled(true);
        presenter.getView().getMitConfirmarFeedBack().setEnabled(true);
        presenter.getView().getMitManterConvites().setEnabled(true);
        presenter.getView().getMitCriarRepublica().setEnabled(false);
        presenter.getView().getMitBuscarVagas().setEnabled(false);
        presenter.getView().getMitConsultaResultadoMensal().setEnabled(false);
        presenter.getView().getMitManterFeedBack().setEnabled(false);
        

        presenter.getView().getMitManterPerfil().setEnabled(false);
        presenter.getView().getMitAceitarConvite().setEnabled(false);

        presenter.getView().getMitManterTarefa().setEnabled(true);
        presenter.getView().getMitRegistrarConclusãoTarefa().setEnabled(false);

        presenter.getView().getMitManterContas().setEnabled(true);
        presenter.getView().getMitRealizarEstorno().setEnabled(true);
        presenter.getView().getMitConsultarContas().setEnabled(false);
        presenter.getView().getMitRegistrarLancamento().setEnabled(false);

        presenter.getView().getMitFechar().setEnabled(true);
    }
    
}
