package com.pss.pssgerenciamentoderepublica.presenter.state.principal;

import com.pss.pssgerenciamentoderepublica.presenter.PrincipalPresenter;
import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public abstract class PrincipalPresenterState {

    protected PrincipalPresenter presenter;

    public PrincipalPresenterState(PrincipalPresenter presenter) {
        this.presenter = presenter;
        limpaListeners();
    }

    private void limpaListeners() {
        JMenuBar menuBar = presenter.getView().getJMenuBar();

        for (Component c : menuBar.getComponents()) {
            if (c instanceof JMenu) {
                for (Component comp : ((JMenu) c).getMenuComponents()) {
                    if (comp instanceof JMenuItem) {
                        for (ActionListener al : ((JMenuItem) comp).getActionListeners()) {
                            ((JMenuItem) comp).removeActionListener(al);
                        }
                    }
                }
            }
        }
    }
    
    
}
