package com.pss.pssgerenciamentoderepublica.presenter;

import com.pss.pssgerenciamentoderepublica.view.NovoUsuarioView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NovoUsuarioPresenter {
    
    private final NovoUsuarioView view;

    public NovoUsuarioPresenter() {
        view = new NovoUsuarioView();
        
        view.setLocationRelativeTo(null);
        view.setVisible(true);
        
        initListeners();
    }

    public NovoUsuarioView getView() {
        return view;
    }
    
    private void initListeners() {
        view.getBtnCadastrar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
            }
        });
        
        view.getBtnCancelar().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cleanFields();
            }
        });
    }
    
    private void cleanFields() {
        view.getTxfNomeUsuario().setText("");
        view.getPwdSenha().setText("");
        view.getPwdConfirmaSenha().setText("");
    }
    
}
