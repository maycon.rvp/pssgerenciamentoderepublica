package com.pss.pssgerenciamentoderepublica.presenter;

import com.pss.pssgerenciamentoderepublica.presenter.state.principal.PrincipalDefaultState;
import com.pss.pssgerenciamentoderepublica.presenter.state.principal.PrincipalPresenterState;
import com.pss.pssgerenciamentoderepublica.model.Usuario;
import com.pss.pssgerenciamentoderepublica.view.PrincipalView;
import javax.swing.JFrame;

public class PrincipalPresenter {
 
    private final PrincipalView view;
    private final Usuario usuario;
    private PrincipalPresenterState estado;


    public PrincipalPresenter(Usuario usuario) {
        this.usuario = usuario;
        view = new PrincipalView();

        view.setExtendedState(JFrame.MAXIMIZED_BOTH);
        view.setVisible(true);

        configurarView();
        this.setEstado(new PrincipalDefaultState(this));
        initListeners();
    }

    private void initListeners() {

    }

    public PrincipalView getView() {
        return view;
    }

    private void configurarView() {

    }

    public void setEstado(PrincipalPresenterState estado) {
        this.estado = estado;
    }
}
